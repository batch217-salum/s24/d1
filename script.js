// console.log("Hello World!");

//[section] exponent operator

const firstNum = 8 ** 2;
console.log(firstNum);

//After ES6 update
const secondNum = Math.pow(8, 2);
console.log(secondNum);

const thirdNum = Math.pow(8, 3);
console.log(thirdNum);

// [section] Template Literals

let name = "John";

// pre-template literal strings
let message = 'Hello ' + name + '! Welcome to programming!';
console.log("Message without template literals: " + message);

// using template literals
//We use backticks(``);
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message without template literals: ${message}`);

const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with solution of ${firstNum}.
`;

console.log(anotherMessage);


const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);


//[section] array destructuring

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's number to meet you!`);

// Array destructuring
const [firstName, middleName, lastName] = fullName

// Using template literal
console.log(`Hello ${firstName} ${middleName} ${lastName}`);


// Object Destructuring

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

//Pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you.`);

// Object destructuring
const {givenName, maidenName, familyName} = person;
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again. `);

// using destructured variable to a function

function getFullName({givenName, maidenName, familyName}){
	console.log(`${person.givenName} ${person.maidenName} ${person.familyName}`);
}

getFullName(person);

// [Section] Arrow Function

/*
Syntax
const variableName = () => {
	console.log()
}
*/

const hello = () => {
	console.log("Hello World!");
}

hello();

// Traditional Function

/*function printFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("John", "Deo", "Smith");*/

// Arrow Function
const printFullName = (firstName,middleName,lastName) =>{
	console.log(`${firstName} ${middleName} ${lastName}`)
}

printFullName("John", "Deo", "Smith");

const students = ["John", "Jane", "Judy"];

//Arrow function with loops
//Pre-arrow function
students.forEach(function(student){
	console.log(`${student} is a student`);
})

// Arrow Function
students.forEach((student) => {
	console.log(`${student} is a student`);
})

// [Section] Default function argument value
const greet = (name = "User") => {
	return `Good morning, ${name}`
}

console.log(greet());
console.log(greet("Charloyd"));
console.log(greet());

// [Section] Class-based object blueprints

//Creating class
class Car {
	constructor(brand, name, year){
		this.brand = brand,
		this.name = name,
		this.year = year
	};
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor"
myCar.year = 2011;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);